package edu.fa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.fa.model.Car;
import edu.fa.repository.CarRepository;

@Service
public class CreateService {
	@Autowired
	private CarRepository carRepository;

	public boolean saveCar(Car car) {
		return (carRepository.save(car) != null);
	}
	
	
}	
