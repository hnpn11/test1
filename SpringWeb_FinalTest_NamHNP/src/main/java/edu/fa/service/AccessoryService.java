package edu.fa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.fa.model.Accessory;
import edu.fa.model.Car;
import edu.fa.repository.AccessoryRepository;

@Service
public class AccessoryService {
	@Autowired
	private AccessoryRepository accessoryRepository;

	public boolean saveAccessory(Accessory accessory) {
		return (accessoryRepository.save(accessory) != null);
	}

	public List<Accessory> findAccessoryByCar(Car car) {
		return accessoryRepository.findAccessoryByCar(car);
	}

	public void saveAccessory(int id) {
		accessoryRepository.delete(id);
	}

	public Accessory getAccessory(int id) {
		return accessoryRepository.findOne(id);
	}
	
	
}
