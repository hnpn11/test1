package edu.fa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.fa.model.Car;
import edu.fa.model.CarId;

public interface CarRepository extends JpaRepository<Car, CarId> {

}
