package edu.fa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.fa.model.Accessory;
import edu.fa.model.Car;

public interface AccessoryRepository extends JpaRepository<Accessory, Integer> {

	List<Accessory> findAccessoryByCar(Car car);

}
