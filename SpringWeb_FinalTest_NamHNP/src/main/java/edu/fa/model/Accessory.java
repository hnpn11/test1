package edu.fa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;

@Entity
public class Accessory {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private double price;
	private String status_damaged;
	private String repair_status;

	@ManyToOne
	@JoinColumns({
        @JoinColumn(
            name = "license_plate",
            referencedColumnName = "license_plate"),
        @JoinColumn(
            name = "repair_date",
            referencedColumnName = "repair_date")
    })
	private Car car;

	public Accessory(String name, double price, String status_damaged, String repair_status, Car car) {
		super();
		this.name = name;
		this.price = price;
		this.status_damaged = status_damaged;
		this.repair_status = repair_status;
		this.car = car;
	}

	public Accessory() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getStatus_damaged() {
		return status_damaged;
	}

	public void setStatus_damaged(String status_damaged) {
		this.status_damaged = status_damaged;
	}

	public String getRepair_status() {
		return repair_status;
	}

	public void setRepair_status(String repair_status) {
		this.repair_status = repair_status;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
