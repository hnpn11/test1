package edu.fa.model;

import java.util.Date;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Car {
	@EmbeddedId
	private CarId carId;

	private String customer_name;
	private String catalog;
	private String car_maker;

	@OneToMany(mappedBy = "car")
	private List<Accessory> accessories;

	public Car(CarId carId, String customer_name, String catalog, String car_maker, List<Accessory> accessories) {
		super();
		this.carId = carId;
		this.customer_name = customer_name;
		this.catalog = catalog;
		this.car_maker = car_maker;
		this.accessories = accessories;
	}

	public Car() {
		super();
		setCarId(new CarId());
	}

	public CarId getCarId() {
		return carId;
	}

	public void setCarId(CarId carId) {
		this.carId = carId;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getCar_maker() {
		return car_maker;
	}

	public void setCar_maker(String car_maker) {
		this.car_maker = car_maker;
	}

	public List<Accessory> getAccessories() {
		return accessories;
	}

	public void setAccessories(List<Accessory> accessories) {
		this.accessories = accessories;
	}

}
