package edu.fa.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Embeddable
public class CarId  implements Serializable {
	private String license_plate;
	@Temporal(TemporalType.DATE)
	@CreationTimestamp
	private Date repair_date;
	public CarId(String license_plate, Date repair_date) {
		super();
		this.license_plate = license_plate;
		this.repair_date = repair_date;
	}
	public CarId() {
		super();
	}
	public String getLicense_plate() {
		return license_plate;
	}
	public void setLicense_plate(String license_plate) {
		this.license_plate = license_plate;
	}
	public Date getRepair_date() {
		return repair_date;
	}
	public void setRepair_date(Date repair_date) {
		this.repair_date = repair_date;
	}
	
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (!(o instanceof CarId)) return false;
	       	CarId that = (CarId) o;
	        return Objects.equals(getLicense_plate(), that.getLicense_plate()) &&
	        		Objects.equals(getRepair_date(), that.getRepair_date());
	    }
	 
	    @Override
	    public int hashCode() {
	        return Objects.hash(getLicense_plate(), getRepair_date());
	    }
}
