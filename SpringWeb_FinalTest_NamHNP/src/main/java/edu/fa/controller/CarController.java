package edu.fa.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.fa.model.Car;
import edu.fa.model.CarId;
import edu.fa.service.CreateService;

@Controller
public class CarController {
	@Autowired
	private CreateService createService;
	
	@RequestMapping("/initCreateCar")
	public String initCreateCar(Model model) {
		Car car = new Car();
		CarId carId = new CarId();
		carId.setRepair_date(new Date());
		car.setCarId(carId);
		model.addAttribute("car", car);
		return "carDetail";
	}
	
	@RequestMapping("/createCar")
	public String createCar(@ModelAttribute() Car car,Model model,HttpServletRequest rq,HttpSession session) throws ParseException{
		car.getCarId().setLicense_plate(rq.getParameter("license_plate"));
		car.getCarId().setRepair_date(new Date());
		if(createService.saveCar(car)) {
			model.addAttribute("error", "Add a new car successfully");
			session.setAttribute("car", car);
			return "redirect:/initUpdateCar";
		} else {
			model.addAttribute("error", "Add a new car failed");
		}
		return "redirect:/initCreateCar";
	}
	
	@RequestMapping("/initUpdateCar")
	public String initUpdate(Model model) {
		return "updateCar";
	}
	
	@RequestMapping("/udpateCar")
	public String updateCar(@ModelAttribute() Car car, HttpServletRequest rq, Model model, HttpSession session) {
		car.getCarId().setLicense_plate(rq.getParameter("license_plate"));
		car.getCarId().setRepair_date(new Date());
		if(createService.saveCar(car)) {
			model.addAttribute("error", "Update  car successfully");
			session.setAttribute("car", car);
		} else {
			model.addAttribute("error", "Update car failed");
		}
		return "redirect:/initUpdateCar";
	}
	
	
	
}
