package edu.fa.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.fa.model.Accessory;
import edu.fa.model.Car;
import edu.fa.service.AccessoryService;

@Controller
public class AccessoryController {
	@Autowired
	private AccessoryService accessoryService;
	
	@RequestMapping("/initAccessory")
	public String initAccessory(HttpSession session) {
		List<Accessory> accessories = accessoryService.findAccessoryByCar((Car) session.getAttribute("car"));
		session.setAttribute("accessories", accessories);
		return "accessory";
	}	
	@RequestMapping("/createAccessory")
	public String createAccessory(@ModelAttribute() Accessory accessory, HttpSession session) {
		accessory.setCar((Car) session.getAttribute("car"));
		accessoryService.saveAccessory(accessory);
		List<Accessory> accessories = accessoryService.findAccessoryByCar((Car) session.getAttribute("car"));
		session.setAttribute("accessories", accessories);
		return "redirect:/initAccessory";
	}
	
	@RequestMapping("/deleteAccessory")
	public String deleteAccessory(@RequestParam("id") int id) {
		accessoryService.saveAccessory(id);
		return "redirect:/initAccessory";
	}
	
	@RequestMapping("/initEditAccessory")
	public String initEditAccessory(@RequestParam("id") int id, HttpSession session){
		session.setAttribute("accessory", accessoryService.getAccessory(id));
		return "updateAccessory";
	}
	
	@RequestMapping("/updateAccessory")
	public String updateAccessory(@ModelAttribute() Accessory accessory, HttpSession session) {
		accessory.setCar((Car) session.getAttribute("car"));
		accessoryService.saveAccessory(accessory);
		List<Accessory> accessories = accessoryService.findAccessoryByCar((Car) session.getAttribute("car"));
		session.setAttribute("accessories", accessories);
		return "redirect:/initAccessory";
	}
	
}
