<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<title>Home Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/style.css"/>">
<style>
#banner {
	border: 1px solid black;
	text-align: center;
}

#menu-left {
	border: 1px solid black;
}

#body {
	border: 1px solid black;
}

#footer {
	border: 1px solid black;
	text-align: center;
}

.cotainer {
	padding: 0px;
}
</style>
</head>
<body>
	<div class="container">
		<div id="banner">
			<h1>AUTOMOTIVE REPAIR</h1>
		</div>
		<div class="row">
			<div id="menu-left" class="col-md-3">
				<p>MENU</p>
				<p style="padding-left: 25px;">
					<a href="<%=request.getContextPath()%>/initCreateCar">Add a new car</a>
				</p>
				<p style="padding-left: 25px;">
					<a href="#">Add a new car</a>
				</p>
				<p style="padding-left: 25px;">
					<a href="#">Add a new car</a>
				</p>
			</div>
			<div id="body" class="col-md-9" style="text-align: center;">
				<h1>WELCOME TO AUTOMOTIVE REPAIR</h1>
			</div>
		</div>
		<div id="footer">
			<h1>BASED ON SPRING FARMWORK</h1>
		</div>
	</div>
</body>
</html>